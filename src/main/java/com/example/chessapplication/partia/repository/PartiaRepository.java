package com.example.chessapplication.partia.repository;

import com.example.chessapplication.partia.domain.Partia;
import com.example.chessapplication.round.domain.Round;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PartiaRepository extends JpaRepository<Partia, Long> {
    Partia findPartiaByName(String name);
}
