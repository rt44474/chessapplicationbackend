package com.example.chessapplication.partia.mappers;

import com.example.chessapplication.partia.domain.Partia;
import com.example.chessapplication.partia.dto.PartiaDto;
import com.example.chessapplication.round.domain.Round;
import com.example.chessapplication.round.dto.RoundDto;
import org.mapstruct.Mapper;

@Mapper
public interface PartiaMappers {
    PartiaDto toDto(Partia partia);
    Partia fromDto(PartiaDto partiaDto);
}
