package com.example.chessapplication.partia.service;

import com.example.chessapplication.partia.dto.PartiaDto;
import com.example.chessapplication.round.dto.RoundDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface PartiaService {
    List<PartiaDto> getAll();
    PartiaDto getById(Long id);
    PartiaDto create(PartiaDto partiaDto);
    PartiaDto edit(PartiaDto partiaDto);
    void delete(Long id);
    PartiaDto saveMatch(Long id, MultipartFile image) throws Exception;
    PartiaDto setWinner(Long id, String winner);
}
