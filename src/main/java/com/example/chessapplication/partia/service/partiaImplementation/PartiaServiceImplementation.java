package com.example.chessapplication.partia.service.partiaImplementation;

import com.example.chessapplication.partia.domain.Partia;
import com.example.chessapplication.partia.dto.PartiaDto;
import com.example.chessapplication.partia.mappers.PartiaMappers;
import com.example.chessapplication.partia.repository.PartiaRepository;
import com.example.chessapplication.partia.service.PartiaService;
import com.example.chessapplication.round.domain.Round;
import com.example.chessapplication.round.dto.RoundDto;
import com.example.chessapplication.round.mappers.RoundMappers;
import com.example.chessapplication.round.repository.RoundRepository;
import com.example.chessapplication.user.Utils.RandomUtil;
import com.example.chessapplication.user.domain.User;
import com.github.bhlangonijr.chesslib.Board;
import com.github.bhlangonijr.chesslib.game.Game;
import com.github.bhlangonijr.chesslib.move.Move;
import com.github.bhlangonijr.chesslib.move.MoveList;
import com.github.bhlangonijr.chesslib.pgn.PgnHolder;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.chessapplication.user.constant.FileConstat.*;
import static com.example.chessapplication.user.constant.UserImpConstant.NO_USER_FOUND_BY_USERNAME;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
@AllArgsConstructor
public class PartiaServiceImplementation implements PartiaService {

    @Autowired
    private PartiaRepository roundRepository;

    protected PartiaMappers roundMappers;
    //private Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    public List<PartiaDto> getAll() {
        return roundRepository.findAll()
                .stream()
                .map(this.roundMappers::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public PartiaDto getById(Long id){
        Optional<Partia> runnerOptional = this.roundRepository.findById(id);

        return this.roundMappers.toDto(
                runnerOptional.get()
        );
    }

    @Override
    public PartiaDto create(PartiaDto raceInputDto) {

        Partia newRunner = this.roundMappers.fromDto(raceInputDto);


        return this.roundMappers.toDto(
                this.roundRepository.save(newRunner)
        );
    }

    @Override
    public PartiaDto saveMatch(Long id, MultipartFile image) throws Exception {
        Optional<Partia> match = this.roundRepository.findById(id);
        Partia partia = match.get();
        partia.setPath(getTemporaryProfileImageUrl(id.toString()));
        this.roundRepository.save(partia);
        saveProfileImage(partia, image);




        return this.roundMappers.toDto(
                partia
        );
    }


    @Override
    public PartiaDto setWinner(Long id, String winner){
        Optional<Partia> match = this.roundRepository.findById(id);
        Partia partia = match.get();
        partia.setWynik(winner);
        this.roundRepository.save(partia);
        return this.roundMappers.toDto(partia);
    }
    private String getTemporaryProfileImageUrl(String username) {
        return ServletUriComponentsBuilder.fromCurrentContextPath().path(DEFAULT_USER_IMAGE_PATH1 + username)
                .toUriString();
    }
    private void saveProfileImage(Partia user, MultipartFile profileImage) throws Exception {
        if (profileImage != null){
            String random = RandomUtil.getRandomMd5();
            Path userFolder = Paths.get(USER_FOLDER1 + random).toAbsolutePath().normalize();
            if (!Files.exists(userFolder)) {
                Files.createDirectories(userFolder);
                System.out.println(DIRECTORY_CREATED + userFolder);
            }

            Files.deleteIfExists(Paths.get(userFolder + user.getName() + DOT + PGN_EXTENSION));
            Files.copy(profileImage.getInputStream(),userFolder.resolve(user.getName()+DOT+PGN_EXTENSION), REPLACE_EXISTING);
            user.setPath(setProfileImageUrl(user.getName(),random));
            //user.setPath(setProfileImageUrl(random));
            roundRepository.save(user);
            System.out.println(FILE_SAVED_IN_FILE_SYSTEM + profileImage.getOriginalFilename());
        }
    }
    private String setProfileImageUrl(String username, String random) {
        return ServletUriComponentsBuilder.fromCurrentContextPath().path(USER_IMAGE_PATH1 + random + FORWARD_SLASH + username + DOT + PGN_EXTENSION)
                .toUriString();
    }
    @Override
    public PartiaDto edit(PartiaDto raceInputDto) {

        Optional<Partia> currentRunnerOptional = this.roundRepository.findById(raceInputDto.getId());


        Partia currentRunner = currentRunnerOptional.get();

        currentRunner.setName(raceInputDto.getName());
        currentRunner.setTempo(raceInputDto.getTempo());
        currentRunner.setStart(raceInputDto.getStart());
        currentRunner.setEnd(raceInputDto.getEnd());
        currentRunner.setPath(raceInputDto.getPath());
        currentRunner.setWynik(raceInputDto.getWynik());



        return this.roundMappers.toDto(
                this.roundRepository.save(currentRunner)
        );
    }

    @Override
    public void delete(Long id) {
        if (!roundRepository.existsById(id)) {
            throw new UsernameNotFoundException(NO_USER_FOUND_BY_USERNAME);
        }

        roundRepository.deleteById(id);
    }
}
