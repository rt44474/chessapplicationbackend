package com.example.chessapplication.partia.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class PartiaUserDto {
    private Long id;
    //private String userId;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String city;
    private String country;
    private String profileImageUrl;
    //private Date lastLoginDate;
    //private Date LastLoginDateDisplay;
    private Date joinDate;
    private String role;
    private String[] authorities;
    private boolean isActive;
    //private boolean isNotLocked;
    private Long ranking;
    private String title;
    private String gender;
    private Long age;
    private double punkty;
    private double zwyciestwa;
    private double remisy;
    private double porazki;
}
