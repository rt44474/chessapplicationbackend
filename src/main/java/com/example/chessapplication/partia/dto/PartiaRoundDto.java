package com.example.chessapplication.partia.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@AllArgsConstructor
@Data
public class PartiaRoundDto {

    private Long id;
    private String name;
    private Date roundStart;
    private Date roundEnd;
    private Long tournament_id;

}
