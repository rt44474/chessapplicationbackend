package com.example.chessapplication.partia.domain;

import com.example.chessapplication.round.domain.Round;
import com.example.chessapplication.tournament.domain.Tournament;
import com.example.chessapplication.user.domain.User;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
public class Partia {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(name="round_id")
    @NonNull
    private Long round_id;

    @ManyToOne(targetEntity = Round.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "round_id", insertable = false, updatable = false)
    private Round round;

    @ManyToMany
    @JoinTable(
            name = "match_user",
            joinColumns = @JoinColumn(name = "match_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> users;

    private String name;
    private double tempo;
    private Date start;
    private Date end;
    private String path;
    private String wynik;
}
