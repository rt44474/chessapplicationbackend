package com.example.chessapplication.partia.controller;

import com.example.chessapplication.partia.dto.PartiaDto;
import com.example.chessapplication.partia.service.PartiaService;
import com.example.chessapplication.round.dto.RoundDto;
import com.example.chessapplication.round.service.RoundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static com.example.chessapplication.user.constant.FileConstat.*;
import static org.springframework.http.MediaType.*;

@RestController
@RequestMapping(value={"/partia"})
public class PartiaController {
    @Autowired
    private PartiaService tournamentService;


    @GetMapping
    public ResponseEntity<List<PartiaDto>> getAll() {
        return ResponseEntity.ok(tournamentService
                .getAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<PartiaDto> getById(@PathVariable Long id) {
        return ResponseEntity.ok(
                tournamentService.getById(id)
        );
    }

    @PostMapping
    //@PreAuthorize("hasAnyAuthority('user:create')")
    public ResponseEntity<PartiaDto> createRace(@RequestBody PartiaDto raceInputDto) {
        return ResponseEntity.ok(
                tournamentService.create(raceInputDto)
        );
    }

    @PutMapping("{id}")
    //@PreAuthorize("hasAnyAuthority('user:edit')")
    public ResponseEntity<PartiaDto> updateRace(@RequestBody PartiaDto raceInputDto, @PathVariable Long id) {
        raceInputDto.setId(id);
        return ResponseEntity.ok(
                tournamentService.edit(raceInputDto)
        );
    }

    @DeleteMapping("{id}")
    //@PreAuthorize("hasAnyAuthority('user:delete')")
    public ResponseEntity<String> deleteRace(@PathVariable Long id) {
        tournamentService.delete(id);

        return ResponseEntity.ok("OK");
    }

    @PatchMapping("/match/{id}")
    public ResponseEntity<PartiaDto> addMatch(@PathVariable Long id, @RequestParam(value = "image") MultipartFile image) throws Exception {
        return ResponseEntity.ok(
                tournamentService.saveMatch(id,image)
        );
    }
    @GetMapping(path = "/match/{username}/{fileName}",produces = TEXT_PLAIN_VALUE)
    public byte[] getProfileImage(@PathVariable("username") String username,
                                  @PathVariable("fileName") String fileName) throws IOException {
        return Files.readAllBytes(Paths.get(USER_FOLDER1 + username + FORWARD_SLASH + fileName));
    }

    @PatchMapping("/winner/{id}")
    public ResponseEntity<PartiaDto> addWinner(@PathVariable Long id, @RequestParam String winner){
        return ResponseEntity.ok(
                tournamentService.setWinner(id,winner)
        );
    }


}
