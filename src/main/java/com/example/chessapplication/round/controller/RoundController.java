package com.example.chessapplication.round.controller;

import com.example.chessapplication.round.domain.Round;
import com.example.chessapplication.round.dto.RoundDto;
import com.example.chessapplication.round.service.RoundService;
import com.example.chessapplication.tournament.domain.Tournament;
import com.example.chessapplication.tournament.dto.TournamentsDto;
import com.example.chessapplication.tournament.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = {"/round"})
public class RoundController {
    @Autowired
    private RoundService tournamentService;


    @GetMapping
    public ResponseEntity<List<RoundDto>> getAll() {
        return ResponseEntity.ok(tournamentService
                .getAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<RoundDto> getById(@PathVariable Long id) {
        return ResponseEntity.ok(
                tournamentService.getById(id)
        );
    }

    @PostMapping
    //@PreAuthorize("hasAnyAuthority('user:create')")
    public ResponseEntity<RoundDto> createRace(@RequestBody RoundDto raceInputDto) {
        return ResponseEntity.ok(
                tournamentService.create(raceInputDto)
        );
    }

    @PutMapping("{id}")
    //@PreAuthorize("hasAnyAuthority('user:edit')")
    public ResponseEntity<RoundDto> updateRace(@RequestBody RoundDto raceInputDto, @PathVariable Long id) {
        raceInputDto.setId(id);
        return ResponseEntity.ok(
                tournamentService.edit(raceInputDto)
        );
    }

    @DeleteMapping("{id}")
    //@PreAuthorize("hasAnyAuthority('user:delete')")
    public ResponseEntity<String> deleteRace(@PathVariable Long id) {
        tournamentService.delete(id);

        return ResponseEntity.ok("OK");
    }
}
