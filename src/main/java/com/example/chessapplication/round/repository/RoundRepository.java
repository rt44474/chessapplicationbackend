package com.example.chessapplication.round.repository;

import com.example.chessapplication.round.domain.Round;
import com.example.chessapplication.tournament.domain.Tournament;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoundRepository extends JpaRepository<Round, Long> {
    Round findRoundByName(String name);
}
