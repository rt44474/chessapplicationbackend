package com.example.chessapplication.round.mappers;

import com.example.chessapplication.round.domain.Round;
import com.example.chessapplication.round.dto.RoundDto;
import org.mapstruct.Mapper;

@Mapper
public interface RoundMappers {
    RoundDto toDto(Round round);
    Round fromDto(RoundDto roundDto);


}
