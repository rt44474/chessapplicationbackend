package com.example.chessapplication.round.domain;

import com.example.chessapplication.partia.domain.Partia;
import com.example.chessapplication.round.dto.RoundDto;
import com.example.chessapplication.round.dto.RoundTournamentDto;
import com.example.chessapplication.tournament.domain.Tournament;
import com.example.chessapplication.user.domain.User;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
public class Round implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    private String name;
    private Date roundStart;
    private Date roundEnd;

    @Column(name="tournament_id")
    @NonNull
    private Long tournament_id;

    @ManyToOne(targetEntity = Tournament.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "tournament_id", insertable = false, updatable = false)
    private Tournament tournament;

    @ManyToMany
    @JoinTable(
            name = "round_user",
            joinColumns = @JoinColumn(name = "round_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> users;

    @OneToMany(mappedBy = "round")
    private List<Partia> partias;

    /*public RoundDto toDto(){
        RoundDto roundDto = new RoundDto();

        roundDto.setId(this.getId());
        roundDto.setName(this.getName());
        roundDto.setRoundStart(this.getRoundStart());
        roundDto.setRoundEnd(this.getRoundEnd());
        roundDto.setTournament_id(this.getTournament_id());


        return roundDto;
    }*/

}
