package com.example.chessapplication.round.dto;

import com.example.chessapplication.tournament.domain.Tournament;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class RoundDto {
    private Long id;
    private String name;
    private Date roundStart;
    private Date roundEnd;
    private Long tournament_id;
    private List<RoundUserDto> users;
    private List<RoundPartiaDto> partias;
    //private RoundTournamentDto tournament;
}
