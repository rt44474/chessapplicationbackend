package com.example.chessapplication.round.dto;

import com.example.chessapplication.round.domain.Round;
import com.example.chessapplication.user.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class RoundTournamentDto {
    private Long id;
    private List<User> users;
    private String name;
    private Date startDate;
    private Date endDate;
    private String city;
    private String country;
    private String address;
    private String system1;
    private Long maxScore;
    private Long minScore;
    private Long numberRounds;
    private Long numberPlayers;
    private boolean finished;
}
