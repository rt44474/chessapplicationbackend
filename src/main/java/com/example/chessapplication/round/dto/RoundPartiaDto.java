package com.example.chessapplication.round.dto;

import com.example.chessapplication.round.domain.Round;
import com.example.chessapplication.user.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
@Data
@AllArgsConstructor
public class RoundPartiaDto {
    private Long id;
    private String name;
    private double tempo;
    private Date start;
    private Date end;
    private String path;
    private String wynik;
}
