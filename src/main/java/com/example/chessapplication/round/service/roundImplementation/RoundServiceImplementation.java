package com.example.chessapplication.round.service.roundImplementation;

import com.example.chessapplication.round.domain.Round;
import com.example.chessapplication.round.dto.RoundDto;
import com.example.chessapplication.round.mappers.RoundMappers;
import com.example.chessapplication.round.repository.RoundRepository;
import com.example.chessapplication.round.service.RoundService;
import com.example.chessapplication.tournament.domain.Tournament;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.chessapplication.user.constant.UserImpConstant.NO_USER_FOUND_BY_USERNAME;

@Service
@AllArgsConstructor
public class RoundServiceImplementation implements RoundService {

    @Autowired
    private RoundRepository roundRepository;

    protected RoundMappers roundMappers;

    @Override
    public List<RoundDto> getAll() {
        return roundRepository.findAll()
                .stream()
                .map(this.roundMappers::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public RoundDto getById(Long id){
        Optional<Round> runnerOptional = this.roundRepository.findById(id);

        return this.roundMappers.toDto(
                runnerOptional.get()
        );
    }

    @Override
    public RoundDto create(RoundDto raceInputDto) {

        Round newRunner = this.roundMappers.fromDto(raceInputDto);


        return this.roundMappers.toDto(
                this.roundRepository.save(newRunner)
        );
    }

    @Override
    public RoundDto edit(RoundDto raceInputDto) {

        Optional<Round> currentRunnerOptional = this.roundRepository.findById(raceInputDto.getId());


        Round currentRunner = currentRunnerOptional.get();

        currentRunner.setName(raceInputDto.getName());
        currentRunner.setRoundStart(raceInputDto.getRoundStart());
        currentRunner.setRoundEnd(raceInputDto.getRoundEnd());


        return this.roundMappers.toDto(
                this.roundRepository.save(currentRunner)
        );
    }

    @Override
    public void delete(Long id) {
        if (!roundRepository.existsById(id)) {
            throw new UsernameNotFoundException(NO_USER_FOUND_BY_USERNAME);
        }

        roundRepository.deleteById(id);
    }
}
