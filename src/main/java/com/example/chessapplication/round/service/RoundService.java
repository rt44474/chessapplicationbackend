package com.example.chessapplication.round.service;

import com.example.chessapplication.round.domain.Round;
import com.example.chessapplication.round.dto.RoundDto;
import com.example.chessapplication.tournament.domain.Tournament;

import java.util.List;

public interface RoundService {
    List<RoundDto> getAll();
    RoundDto getById(Long id);
    RoundDto create(RoundDto round);
    RoundDto edit(RoundDto round);
    void delete(Long id);
}
