package com.example.chessapplication.tournament.dto;

import com.example.chessapplication.user.domain.User;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class TournamentInputDto {
    private Long id;
    private List<User> users;
    private String name;
    private Date startDate;
    private Date endDate;
    private String city;
    private String country;
    private String address;
    private String system1;
    private Long maxScore;
    private Long minScore;
    private Long numberRounds;
    private Long numberPlayers;
    private boolean finished;
}
