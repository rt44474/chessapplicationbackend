package com.example.chessapplication.tournament.dto;

import com.example.chessapplication.tournament.domain.Tournament;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;
@Getter
@Setter
public class TournamentsRoundDto {
    private Long id;
    private String name;
    private Date roundStart;
    private Date roundEnd;

}
