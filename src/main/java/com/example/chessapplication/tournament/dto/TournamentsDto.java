package com.example.chessapplication.tournament.dto;

import com.example.chessapplication.user.domain.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.Date;
import java.util.List;
@Getter
@Setter
public class TournamentsDto {
    private Long id;
    private List<TournamentsUserDto> users;
    private List<TournamentsUserDto> acceptedUsers;
    private List<TournamentsRoundDto> rounds;
    private String name;
    private Date startDate;
    private Date endDate;
    private String city;
    private String country;
    private String address;
    private String system1;
    private Long maxScore;
    private Long minScore;
    private Long numberRounds;
    private Long numberPlayers;
    private boolean isStarted;
    private boolean finished;
    private String description;
    private boolean accepted;
    private String organizerId;
    private String judgeId;
    private String type;
    private String winner;
    private String second;
    private String third;

}
