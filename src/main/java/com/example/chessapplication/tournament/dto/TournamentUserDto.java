package com.example.chessapplication.tournament.dto;

import lombok.Data;

@Data
public class TournamentUserDto {
    private Long id;
    private String username;
    private String role;
}
