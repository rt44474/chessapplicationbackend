package com.example.chessapplication.tournament.controller;

import com.example.chessapplication.tournament.domain.Tournament;
import com.example.chessapplication.tournament.dto.TournamentDto;
import com.example.chessapplication.tournament.dto.TournamentInputDto;
import com.example.chessapplication.tournament.dto.TournamentsDto;
import com.example.chessapplication.tournament.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = {"/tournament"})
public class TournamentController {

    @Autowired
    private TournamentService tournamentService;


    @GetMapping
    public ResponseEntity<List<TournamentsDto>> getAll() {
        return ResponseEntity.ok(tournamentService
                .getAll()
                .stream()
                .map(tournament -> tournament.toDto2())
                .collect(Collectors.toList()));
    }

    @GetMapping("{id}")
    public ResponseEntity<TournamentsDto> getById(@PathVariable Long id) {
        return ResponseEntity.ok(
                tournamentService.getById(id).toDto2()
        );
    }

    @PostMapping
    //@PreAuthorize("hasAnyAuthority('user:create')")
    public ResponseEntity<Tournament> createRace(@RequestBody Tournament raceInputDto) {
        return ResponseEntity.ok(
                tournamentService.create(raceInputDto)
        );
    }

    @PutMapping("{id}")
    //@PreAuthorize("hasAnyAuthority('user:edit')")
    public ResponseEntity<TournamentsDto> updateRace(@RequestBody Tournament raceInputDto, @PathVariable Long id) {
        raceInputDto.setId(id);
        return ResponseEntity.ok(
                tournamentService.edit(raceInputDto).toDto1()
        );
    }

    @PatchMapping("/accept/{id}/{uId}")
    public ResponseEntity<TournamentsDto> acceptPlayer(@RequestBody Tournament raceInputDto, @PathVariable Long id, @PathVariable Long uId) {
        //raceInputDto.setId(id);

        System.out.println("uId "+uId);
        return ResponseEntity.ok(
                tournamentService.acceptPlayer(raceInputDto,id, uId).toDto2()
        );
    }

    @PatchMapping("/deny/{id}/{uId}")
    public ResponseEntity<TournamentsDto> denyPlayer(@RequestBody Tournament raceInputDto, @PathVariable Long id, @PathVariable Long uId) {
        //raceInputDto.setId(id);

        System.out.println("uId "+uId);
        return ResponseEntity.ok(
                tournamentService.denyPlayer(raceInputDto,id, uId).toDto2()
        );
    }

    @PatchMapping("/addWinner/{id}")
    public ResponseEntity<TournamentsDto> addWinner(@PathVariable Long id, @RequestParam String winner, @RequestParam String second, @RequestParam String third){
        return ResponseEntity.ok(
                tournamentService.addWinner(id, winner, second, third).toDto2()
        );
    }

    @DeleteMapping("{id}")
    //@PreAuthorize("hasAnyAuthority('user:delete')")
    public ResponseEntity<String> deleteRace(@PathVariable Long id) {
        tournamentService.delete(id);

        return ResponseEntity.ok("OK");
    }
}
