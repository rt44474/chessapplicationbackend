package com.example.chessapplication.tournament.repository;

import com.example.chessapplication.tournament.domain.Tournament;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TournamentRepository extends JpaRepository<Tournament, Long> {
    Tournament findTournamentByName(String name);
    Tournament findTournamentById(Long id);

}
