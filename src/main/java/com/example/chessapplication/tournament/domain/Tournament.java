package com.example.chessapplication.tournament.domain;

import com.example.chessapplication.round.domain.Round;
import com.example.chessapplication.tournament.dto.TournamentsDto;
import com.example.chessapplication.tournament.dto.TournamentsRoundDto;
import com.example.chessapplication.tournament.dto.TournamentsUserDto;
import com.example.chessapplication.user.domain.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
public class Tournament  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    @ManyToMany
    @JoinTable(
            name = "tournament_user",
            joinColumns = @JoinColumn(name = "tournament_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> users;
    @ManyToMany
    @JoinTable(
            name = "tournament_user_accepted",
            joinColumns = @JoinColumn(name = "tournament_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> acceptedUsers;
    @OneToMany(mappedBy = "tournament")
    private List<Round> rounds;

    private String name;
    private Date startDate;
    private Date endDate;
    private String city;
    private String country;
    private String address;
    private String system1;
    private Long maxScore;
    private Long minScore;
    private Long numberRounds;
    private Long numberPlayers;
    private boolean isStarted;
    private boolean finished;
    private String description;
    private boolean accepted;
    private String organizerId;
    private String judgeId;
    private String type;
    private String winner;
    private String second;
    private String third;
    public TournamentsDto toDto2(){
        TournamentsDto tournamentsDto = new TournamentsDto();

        tournamentsDto.setId(this.getId());
        tournamentsDto.setName(this.getName());
        tournamentsDto.setStartDate(this.getStartDate());
        tournamentsDto.setEndDate(this.getEndDate());
        tournamentsDto.setCity(this.getCity());
        tournamentsDto.setCountry(this.getCountry());
        tournamentsDto.setAddress(this.getAddress());
        tournamentsDto.setSystem1(this.getSystem1());
        tournamentsDto.setMaxScore(this.getMaxScore());
        tournamentsDto.setMinScore(this.getMinScore());
        tournamentsDto.setNumberRounds(this.getNumberRounds());
        tournamentsDto.setNumberPlayers(this.getNumberPlayers());
        tournamentsDto.setStarted(this.isStarted());
        tournamentsDto.setFinished(this.isFinished());
        tournamentsDto.setDescription(this.getDescription());
        tournamentsDto.setAccepted(this.isAccepted());
        tournamentsDto.setOrganizerId(this.getOrganizerId());
        tournamentsDto.setJudgeId(this.getJudgeId());
        tournamentsDto.setType(this.getType());
        tournamentsDto.setWinner(this.getWinner());
        tournamentsDto.setSecond(this.getSecond());
        tournamentsDto.setThird(this.getThird());
        //System.out.println(this.getUsers().size());
        List<TournamentsUserDto> tournamentsUserDtoList = this.getUsers()
                .stream()
                .map(user -> {
                    TournamentsUserDto tournamentsUserDto = new TournamentsUserDto();
                    tournamentsUserDto.setId(user.getId());
                    //System.out.println("a1");
                    //tournamentsUserDto.setUserId(user.getUserId());
                    //tournamentsUserDto.setPassword(user.getPassword());
                    tournamentsUserDto.setFirstName(user.getFirstName());
                    tournamentsUserDto.setLastName(user.getLastName());
                    tournamentsUserDto.setEmail(user.getEmail());
                    tournamentsUserDto.setPhoneNumber(user.getPhoneNumber());
                    tournamentsUserDto.setCity(user.getCity());
                    tournamentsUserDto.setCountry(user.getCountry());
                    tournamentsUserDto.setProfileImageUrl(user.getProfileImageUrl());
                    //tournamentsUserDto.setLastLoginDate(user.getLastLoginDate());
                    tournamentsUserDto.setJoinDate(user.getJoinDate());
                    tournamentsUserDto.setRole(user.getRole());
                    tournamentsUserDto.setAuthorities(user.getAuthorities());
                    tournamentsUserDto.setActive(user.isActive());
                    tournamentsUserDto.setUsername(user.getUsername());
                    tournamentsUserDto.setGender(user.getGender());
                    //tournamentsUserDto.setNotLocked(user.isNotLocked());
                    tournamentsUserDto.setRanking(user.getRanking());
                    tournamentsUserDto.setTitle(user.getTitle());
                    tournamentsUserDto.setAge(user.getAge());
                    tournamentsUserDto.setPunkty(user.getPunkty());
                    tournamentsUserDto.setZwyciestwa(user.getZwyciestwa());
                    tournamentsUserDto.setRemisy(user.getRemisy());
                    tournamentsUserDto.setPorazki(user.getPorazki());
                    //System.out.println("a2");
                    return tournamentsUserDto;
                }).collect(Collectors.toList());
        tournamentsDto.setUsers(tournamentsUserDtoList);
        List<TournamentsUserDto> tournamentsUserDtoList1 = this.getAcceptedUsers()
                .stream()
                .map(user -> {
                    TournamentsUserDto tournamentsUserDto = new TournamentsUserDto();
                    tournamentsUserDto.setId(user.getId());
                    //System.out.println("a1");
                    //tournamentsUserDto.setUserId(user.getUserId());
                    //tournamentsUserDto.setPassword(user.getPassword());
                    tournamentsUserDto.setFirstName(user.getFirstName());
                    tournamentsUserDto.setLastName(user.getLastName());
                    tournamentsUserDto.setEmail(user.getEmail());
                    tournamentsUserDto.setPhoneNumber(user.getPhoneNumber());
                    tournamentsUserDto.setCity(user.getCity());
                    tournamentsUserDto.setCountry(user.getCountry());
                    tournamentsUserDto.setProfileImageUrl(user.getProfileImageUrl());
                    //tournamentsUserDto.setLastLoginDate(user.getLastLoginDate());
                    tournamentsUserDto.setJoinDate(user.getJoinDate());
                    tournamentsUserDto.setRole(user.getRole());
                    tournamentsUserDto.setAuthorities(user.getAuthorities());
                    tournamentsUserDto.setUsername(user.getUsername());
                    tournamentsUserDto.setGender(user.getGender());
                    tournamentsUserDto.setActive(user.isActive());
                    //tournamentsUserDto.setNotLocked(user.isNotLocked());
                    tournamentsUserDto.setRanking(user.getRanking());
                    tournamentsUserDto.setTitle(user.getTitle());
                    tournamentsUserDto.setAge(user.getAge());
                    tournamentsUserDto.setPunkty(user.getPunkty());
                    tournamentsUserDto.setZwyciestwa(user.getZwyciestwa());
                    tournamentsUserDto.setRemisy(user.getRemisy());
                    tournamentsUserDto.setPorazki(user.getPorazki());
                    //System.out.println("a2");
                    return tournamentsUserDto;
                }).collect(Collectors.toList());
        tournamentsDto.setAcceptedUsers(tournamentsUserDtoList1);
        List<TournamentsRoundDto> tournamentsRoundDtoList = this.getRounds()
                .stream()
                .map(round -> {
                    TournamentsRoundDto tournamentsRoundDto = new TournamentsRoundDto();
                    tournamentsRoundDto.setId(round.getId());
                    //System.out.println(round.getId());
                    tournamentsRoundDto.setName(round.getName());
                    //System.out.println(round.getName());
                    tournamentsRoundDto.setRoundStart(round.getRoundStart());
                    //System.out.println(round.getRoundEnd());
                    tournamentsRoundDto.setRoundEnd(round.getRoundEnd());

                    //System.out.println(round.getRoundStart());
                    return tournamentsRoundDto;
                }).collect(Collectors.toList());
        tournamentsDto.setRounds(tournamentsRoundDtoList);
        return tournamentsDto;


    }
    public TournamentsDto toDto1(){
        TournamentsDto tournamentsDto = new TournamentsDto();

        tournamentsDto.setId(this.getId());
        tournamentsDto.setName(this.getName());
        tournamentsDto.setStartDate(this.getStartDate());
        tournamentsDto.setEndDate(this.getEndDate());
        tournamentsDto.setCity(this.getCity());
        tournamentsDto.setCountry(this.getCountry());
        tournamentsDto.setAddress(this.getAddress());
        tournamentsDto.setSystem1(this.getSystem1());
        tournamentsDto.setMaxScore(this.getMaxScore());
        tournamentsDto.setMinScore(this.getMinScore());
        tournamentsDto.setNumberRounds(this.getNumberRounds());
        tournamentsDto.setNumberPlayers(this.getNumberPlayers());
        tournamentsDto.setStarted(this.isStarted());
        tournamentsDto.setFinished(this.isFinished());
        tournamentsDto.setDescription(this.getDescription());
        tournamentsDto.setAccepted(this.isAccepted());
        tournamentsDto.setOrganizerId(this.getOrganizerId());
        tournamentsDto.setJudgeId(this.getJudgeId());
        tournamentsDto.setType(this.getType());
        tournamentsDto.setWinner(this.getWinner());
        tournamentsDto.setSecond(this.getSecond());
        tournamentsDto.setThird(this.getThird());
        //System.out.println(this.getUsers().size());
        List<TournamentsUserDto> tournamentsUserDtoList = this.getUsers()
                .stream()
                .map(user -> {
                    TournamentsUserDto tournamentsUserDto = new TournamentsUserDto();
                    tournamentsUserDto.setId(user.getId());
                    //System.out.println("a1");
                    //tournamentsUserDto.setUserId(user.getUserId());
                    //tournamentsUserDto.setPassword(user.getPassword());
                    tournamentsUserDto.setFirstName(user.getFirstName());
                    tournamentsUserDto.setLastName(user.getLastName());
                    tournamentsUserDto.setEmail(user.getEmail());
                    tournamentsUserDto.setPhoneNumber(user.getPhoneNumber());
                    tournamentsUserDto.setCity(user.getCity());
                    tournamentsUserDto.setCountry(user.getCountry());
                    tournamentsUserDto.setProfileImageUrl(user.getProfileImageUrl());
                    //tournamentsUserDto.setLastLoginDate(user.getLastLoginDate());
                    tournamentsUserDto.setJoinDate(user.getJoinDate());
                    tournamentsUserDto.setUsername(user.getUsername());
                    tournamentsUserDto.setGender(user.getGender());
                    tournamentsUserDto.setRole(user.getRole());
                    tournamentsUserDto.setAuthorities(user.getAuthorities());
                    tournamentsUserDto.setActive(user.isActive());
                    //tournamentsUserDto.setNotLocked(user.isNotLocked());
                    tournamentsUserDto.setRanking(user.getRanking());
                    tournamentsUserDto.setTitle(user.getTitle());
                    tournamentsUserDto.setAge(user.getAge());
                    tournamentsUserDto.setPunkty(user.getPunkty());
                    tournamentsUserDto.setZwyciestwa(user.getZwyciestwa());
                    tournamentsUserDto.setRemisy(user.getRemisy());
                    tournamentsUserDto.setPorazki(user.getPorazki());
                    //System.out.println("a2");
                    return tournamentsUserDto;
                }).collect(Collectors.toList());
        tournamentsDto.setUsers(tournamentsUserDtoList);
        return tournamentsDto;


    }
    public TournamentsDto toDto(){
        TournamentsDto tournamentsDto = new TournamentsDto();

        tournamentsDto.setId(this.getId());
        tournamentsDto.setName(this.getName());
        tournamentsDto.setStartDate(this.getStartDate());
        tournamentsDto.setEndDate(this.getEndDate());
        tournamentsDto.setCity(this.getCity());
        tournamentsDto.setCountry(this.getCountry());
        tournamentsDto.setAddress(this.getAddress());
        tournamentsDto.setSystem1(this.getSystem1());
        tournamentsDto.setMaxScore(this.getMaxScore());
        tournamentsDto.setMinScore(this.getMinScore());
        tournamentsDto.setNumberRounds(this.getNumberRounds());
        tournamentsDto.setNumberPlayers(this.getNumberPlayers());
        tournamentsDto.setStarted(this.isStarted());
        tournamentsDto.setFinished(this.isFinished());
        tournamentsDto.setDescription(this.getDescription());
        tournamentsDto.setAccepted(this.isAccepted());
        tournamentsDto.setOrganizerId(this.getOrganizerId());
        tournamentsDto.setJudgeId(this.getJudgeId());
        tournamentsDto.setType(this.getType());
        tournamentsDto.setWinner(this.getWinner());
        tournamentsDto.setSecond(this.getSecond());
        tournamentsDto.setThird(this.getThird());
        //System.out.println(this.getUsers().size());
        List<TournamentsUserDto> tournamentsUserDtoList = this.getUsers()
                .stream()
                .map(user -> {
                    TournamentsUserDto tournamentsUserDto = new TournamentsUserDto();
                    tournamentsUserDto.setId(user.getId());
                    //System.out.println("a1");
                    //tournamentsUserDto.setUserId(user.getUserId());
                    //tournamentsUserDto.setPassword(user.getPassword());
                    tournamentsUserDto.setFirstName(user.getFirstName());
                    tournamentsUserDto.setLastName(user.getLastName());
                    tournamentsUserDto.setEmail(user.getEmail());
                    tournamentsUserDto.setPhoneNumber(user.getPhoneNumber());
                    tournamentsUserDto.setCity(user.getCity());
                    tournamentsUserDto.setCountry(user.getCountry());
                    tournamentsUserDto.setProfileImageUrl(user.getProfileImageUrl());
                    //tournamentsUserDto.setLastLoginDate(user.getLastLoginDate());
                    tournamentsUserDto.setJoinDate(user.getJoinDate());
                    tournamentsUserDto.setRole(user.getRole());
                    tournamentsUserDto.setAuthorities(user.getAuthorities());
                    tournamentsUserDto.setUsername(user.getUsername());
                    tournamentsUserDto.setGender(user.getGender());
                    tournamentsUserDto.setActive(user.isActive());
                    //tournamentsUserDto.setNotLocked(user.isNotLocked());
                    tournamentsUserDto.setRanking(user.getRanking());
                    tournamentsUserDto.setTitle(user.getTitle());
                    tournamentsUserDto.setAge(user.getAge());
                    tournamentsUserDto.setPunkty(user.getPunkty());
                    tournamentsUserDto.setZwyciestwa(user.getZwyciestwa());
                    tournamentsUserDto.setRemisy(user.getRemisy());
                    tournamentsUserDto.setPorazki(user.getPorazki());
                    //System.out.println("a2");
                    return tournamentsUserDto;
                }).collect(Collectors.toList());
        tournamentsDto.setUsers(tournamentsUserDtoList);
        //System.out.println("a3");

        List<TournamentsRoundDto> tournamentsRoundDtoList = this.getRounds()
                .stream()
                .map(round -> {
                    TournamentsRoundDto tournamentsRoundDto = new TournamentsRoundDto();
                    tournamentsRoundDto.setId(round.getId());
                    //System.out.println(round.getId());
                    tournamentsRoundDto.setName(round.getName());
                    //System.out.println(round.getName());
                    tournamentsRoundDto.setRoundStart(round.getRoundStart());
                    //System.out.println(round.getRoundEnd());
                    tournamentsRoundDto.setRoundEnd(round.getRoundEnd());
                    //System.out.println(round.getRoundStart());
                    return tournamentsRoundDto;
                }).collect(Collectors.toList());
        tournamentsDto.setRounds(tournamentsRoundDtoList);
        //System.out.println("a4");
        return tournamentsDto;
    }
}
