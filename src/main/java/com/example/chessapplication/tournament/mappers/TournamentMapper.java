package com.example.chessapplication.tournament.mappers;
import com.example.chessapplication.tournament.domain.Tournament;
import com.example.chessapplication.tournament.dto.TournamentDto;
import com.example.chessapplication.tournament.dto.TournamentInputDto;
import org.mapstruct.Mapper;


@Mapper
public interface TournamentMapper {
    TournamentDto toDto(Tournament tournament);
    Tournament fromDto(TournamentInputDto tournamentInputDto);
}
