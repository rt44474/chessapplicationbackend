package com.example.chessapplication.tournament.service.tournamentImplementation;

import com.example.chessapplication.tournament.domain.Tournament;
import com.example.chessapplication.tournament.dto.TournamentDto;
import com.example.chessapplication.tournament.dto.TournamentInputDto;
import com.example.chessapplication.tournament.mappers.TournamentMapper;
import com.example.chessapplication.tournament.repository.TournamentRepository;
import com.example.chessapplication.tournament.service.TournamentService;
import com.example.chessapplication.user.domain.User;
import com.example.chessapplication.user.exception.domain.EmailExistException;
import com.example.chessapplication.user.exception.domain.EmailNotFoundException;
import javassist.NotFoundException;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.chessapplication.user.constant.UserImpConstant.NO_USER_FOUND_BY_USERNAME;

@Service
public class TournamentServiceImplementation implements TournamentService{

    @Autowired
    private TournamentRepository tournamentRepository;

    private TournamentMapper tournamentMapper = Mappers.getMapper(TournamentMapper.class);

    @Override
    public List<Tournament> getAll() {
        return tournamentRepository.findAll();
    }

    @Override
    public Tournament getById(Long id){
        return tournamentRepository.getById(id);
    }

    @Override
    public Tournament create(Tournament raceInputDto) {
        System.out.println(raceInputDto.getNumberPlayers());
        return tournamentRepository.save(raceInputDto);
    }

    @Override
    public Tournament edit(Tournament raceInputDto) {
        Tournament tournament = new Tournament();
        tournament = raceInputDto;
        tournamentRepository.save(tournament);
        return tournament;
    }

    @Override
    public Tournament acceptPlayer(Tournament tournament,Long tId, Long id){
        System.out.println("id: "+id);
        Tournament tournament1 = tournamentRepository.findTournamentById(tId);

        //tournament1 = tournament;
        //System.out.println("size: "+tournament1.getPendingUsers().size());
        List<User> userList = tournament1.getUsers();
        for (User u : userList){
            if(u.getId().equals(id)){
                tournament1.getAcceptedUsers().add(u);
                tournament1.getUsers().remove(u);
                break;
            }
        }
        tournamentRepository.save(tournament1);
        return tournament1;
    }
    @Override
    public Tournament denyPlayer(Tournament tournament,Long tId, Long id){
        System.out.println("id: "+id);
        Tournament tournament1 = tournamentRepository.findTournamentById(tId);

        //tournament1 = tournament;
        //System.out.println("size: "+tournament1.getPendingUsers().size());
        List<User> userList = tournament1.getUsers();
        for (User u : userList){
            if(u.getId().equals(id)){
                tournament1.getAcceptedUsers().remove(u);
                tournament1.getUsers().remove(u);
                break;
            }
        }
        tournamentRepository.save(tournament1);
        return tournament1;
    }
    @Override
    public Tournament addWinner(Long id, String winner, String second, String third){
        Tournament tournament = tournamentRepository.findTournamentById(id);

        tournament.setWinner(winner);
        tournament.setSecond(second);
        tournament.setThird(third);
        tournamentRepository.save(tournament);
        return tournament;
    }
    @Override
    public void delete(Long id) {
        if (!tournamentRepository.existsById(id)) {
            throw new UsernameNotFoundException(NO_USER_FOUND_BY_USERNAME);
        }

        tournamentRepository.deleteById(id);
    }
}
