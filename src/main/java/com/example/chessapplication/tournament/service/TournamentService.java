package com.example.chessapplication.tournament.service;

import com.example.chessapplication.tournament.domain.Tournament;
import com.example.chessapplication.tournament.dto.TournamentDto;
import com.example.chessapplication.tournament.dto.TournamentInputDto;

import java.util.List;

public interface TournamentService {
    List<Tournament> getAll();
    Tournament getById(Long id);
    Tournament create(Tournament tournament);
    Tournament edit(Tournament tournament);
    Tournament acceptPlayer(Tournament tournament,Long tId, Long id);
    Tournament denyPlayer(Tournament tournament, Long tId, Long id);
    Tournament addWinner(Long id, String winner, String second, String third);
    void delete(Long id);
}
