package com.example.chessapplication.user.constant;

public class SecurityConstant {

    public static final long EXPIRATION_TIME = 3_600_000; //1 hour
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String JWT_TOKEN_HEADER = "Jwt-Token";
    public static final String TOKEN_CANNOT_BE_VERIFIED = "Token cannot be verified";
    public static final String GET_ARRAYS_LLC = "Get Arrays, LLC";
    public static final String GET_ARRAYS_ADMINISTRATION = "User Managment Portal";
    public static final String AUTHORITIES = "Authorities";
    public static final String FORBIDDEN_MESSAGE = "You need to log in to access this page";
    public static final String ACCESS_DENIED_MESSAGE = "You do not have permission to access this page";
    public static final String OPTIONS_HTTP_METHOD = "OPTIONS";
    public static final String[] PUBLIC_URLS = {"/user/login","/user/register","/user/resetpassword/**","/user/image/**","/tournament","/tournament/*","/tournaemtn/**","/user/list","/user/*","/user/**","/round","/round/*","/round/**","/delete/*","/delete","/delete/**","/partia","/partia/*","/partia/**","/tournament/accept/","/accept/","/accept/*","/accept/**","/tournament/accept/*","/tournament/accept/**","/tournament/accept/*/*","/tournament/accept/*/**","/tournament/accept/**/*","/tournament/addWinner/","/tournament/addWinner/*","/tournament/addWinner/**","/user/verify/**","/user/verify","/user/verify/*","/user/**","/user/*"};

}
