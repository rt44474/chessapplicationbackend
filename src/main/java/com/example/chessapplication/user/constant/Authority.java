package com.example.chessapplication.user.constant;

public class Authority {
    //AUTHORITIES TO USERS
    public static final String[] USER_AUTHORITIES = { "user:read" };
    public static final String[] ORGANISER_AUTHORITIES = { "user:read" };
    public static final String[] ADMIN_AUTHORITIES = { "user:read","user:update","user:create", "user:delete" };
    public static final String[] SEDZIA_AUTHORITIES = { "user:read"};



}
