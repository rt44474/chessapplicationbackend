package com.example.chessapplication.user.constant;

public class FileConstat {
    public static final String USER_IMAGE_PATH = "/user/image/";
    public static final String USER_IMAGE_PATH1 = "/partia/match/";
    public static final String JPG_EXTENSION = "jpg";
    public static final String PGN_EXTENSION = "pgn";
    public static final String USER_FOLDER = System.getProperty("user.home") + "/supportportal/user/";
    public static final String USER_FOLDER1 = System.getProperty("user.home") + "/supportportal/matches/";
    public static final String DIRECTORY_CREATED = "Created directory for: ";
    public static final String DEFAULT_USER_IMAGE_PATH = "/user/image/profile/";
    public static final String DEFAULT_USER_IMAGE_PATH1 = "/partia/match/profile/";
    public static final String FILE_SAVED_IN_FILE_SYSTEM = "Saved file in file system by name: ";
    public static final String DOT = ".";
    public static final String FORWARD_SLASH = "/";
    public static final String TEMP_PROFILE_IMAGE_BASE_URL = "https://robohash.org/";



}
