package com.example.chessapplication.user.listener;

import com.example.chessapplication.user.service.LoginAttemptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;

@Component
public class AutenticationFailureListener {
    private LoginAttemptService loginAttemptService;

    @Autowired
    public AutenticationFailureListener(LoginAttemptService loginAttemptService) {
        this.loginAttemptService = loginAttemptService;
    }

    @EventListener
    public void onAuthenticationFailure(AuthenticationFailureBadCredentialsEvent authenticationFailureBadCredentialsEvent) throws ExecutionException {
        Object principal = authenticationFailureBadCredentialsEvent.getAuthentication().getPrincipal();
        if(principal instanceof String) {
            String username = (String) authenticationFailureBadCredentialsEvent.getAuthentication().getPrincipal();
            loginAttemptService.addUserToLoginAttemptCache(username);
        }
    }
}
