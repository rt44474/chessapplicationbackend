package com.example.chessapplication.user.dto;

import com.example.chessapplication.round.domain.Round;
import com.example.chessapplication.user.domain.User;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class UserPartiaDto {
    private Long id;
    private Long round_id;
    private String name;
    private double tempo;
    private Date start;
    private Date end;
    private String path;
    private String wynik;
}
