package com.example.chessapplication.user.dto;

import com.example.chessapplication.tournament.domain.Tournament;
import com.example.chessapplication.user.domain.User;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
@Getter
@Setter
public class UserRoundDto {
    private Long id;
    private String name;
    private Date roundStart;
    private Date roundEnd;
    private Long tournament_id;
}
