package com.example.chessapplication.user.dto;

import com.example.chessapplication.tournament.domain.Tournament;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.ManyToMany;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class UserDto {
    private Long id;
    private List<UserTournamentDto> tournaments;
    private List<UserTournamentDto> registeredTournaments;
    private List<UserRoundDto> rounds;
    private List<UserPartiaDto> partias;
    //private String userId;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String city;
    private String country;
    private String profileImageUrl;
    //private Date lastLoginDate;
    //private Date LastLoginDateDisplay;
    private Date joinDate;
    private String role;
    private String[] authorities;
    private boolean isActive;
    //private boolean isNotLocked;
    private Long ranking;
    private String title;
    private String gender;
    private Long age;
    private double punkty;
    private double zwyciestwa;
    private double remisy;
    private double porazki;
}
