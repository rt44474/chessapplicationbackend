package com.example.chessapplication.user.dto;

import com.example.chessapplication.tournament.domain.Tournament;
import com.example.chessapplication.user.domain.User;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class UserTournamentDto {
    private Long id;
    private String name;
    private Date startDate;
    private Date endDate;
    private String city;
    private String country;
    private String address;
    private String system1;
    private Long maxScore;
    private Long minScore;
    private Long numberRounds;
    private Long numberPlayers;
    private boolean finished;
}
