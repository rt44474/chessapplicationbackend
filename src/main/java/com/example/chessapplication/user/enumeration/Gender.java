package com.example.chessapplication.user.enumeration;

public enum Gender {
    WOMAN,
    MAN;
}
