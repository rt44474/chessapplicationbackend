package com.example.chessapplication.user.enumeration;

import static com.example.chessapplication.user.constant.Authority.*;

public enum Role {
    ROLE_ADMIN(ADMIN_AUTHORITIES),
    ROLE_ORGANISER(ORGANISER_AUTHORITIES),
    ROLE_JUDGE(SEDZIA_AUTHORITIES),
    ROLE_USER(USER_AUTHORITIES);

    private String[] authorities;

    Role(String... authorities) {
        this.authorities = authorities;
    }

    public String[] getAuthorities() {
        return authorities;
    }
}
