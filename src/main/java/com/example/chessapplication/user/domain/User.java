package com.example.chessapplication.user.domain;

import com.example.chessapplication.partia.domain.Partia;
import com.example.chessapplication.round.domain.Round;
import com.example.chessapplication.tournament.domain.Tournament;
import com.example.chessapplication.user.dto.UserDto;
import com.example.chessapplication.user.dto.UserPartiaDto;
import com.example.chessapplication.user.dto.UserRoundDto;
import com.example.chessapplication.user.dto.UserTournamentDto;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    @ManyToMany(mappedBy = "users")
    private List<Tournament> tournaments;

    @ManyToMany(mappedBy = "acceptedUsers")
    private List<Tournament> registeredTournaments;

    @ManyToMany(mappedBy = "users")
    private List<Round> rounds;

    @ManyToMany(mappedBy = "users")
    private List<Partia> partias;

    //private String userId;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String city;
    private String country;
    private String profileImageUrl;
    //private Date lastLoginDate;
    //private Date LastLoginDateDisplay;
    private Date joinDate;
    private String role;
    private String[] authorities;
    private boolean isActive;
    //private boolean isNotLocked;
    private Long ranking;
    private String title;
    private String gender;
    private Long age;
    private double punkty;
    private double zwyciestwa;
    private double remisy;
    private double porazki;

    //TODO:
    //TURNIEJE, MECZE

    public UserDto toDto(){
        UserDto userDto = new UserDto();

        userDto.setId(this.getId());
        //userDto.setUserId(this.getUserId());
        userDto.setPassword(this.getPassword());
        userDto.setFirstName(this.getFirstName());
        userDto.setUsername(this.getUsername());
        userDto.setLastName(this.getLastName());
        userDto.setEmail(this.getEmail());
        userDto.setPhoneNumber(this.getPhoneNumber());
        userDto.setCity(this.getCity());
        userDto.setCountry(this.getCountry());
        userDto.setProfileImageUrl(this.getProfileImageUrl());
        //userDto.setLastLoginDate(this.getLastLoginDate());
        userDto.setJoinDate(this.getJoinDate());
        userDto.setRole(this.getRole());
        userDto.setGender(this.getGender());
        userDto.setAuthorities(this.getAuthorities());
        userDto.setActive(this.isActive());
        //userDto.setNotLocked(this.isNotLocked());
        userDto.setRanking(this.getRanking());
        userDto.setTitle(this.getTitle());
        userDto.setAge(this.getAge());
        userDto.setPunkty(this.getPunkty());
        userDto.setZwyciestwa(this.getZwyciestwa());
        userDto.setRemisy(this.getRemisy());
        userDto.setPorazki(this.getPorazki());

        List<UserTournamentDto> userTournamentDtoList= this.getTournaments()
                .stream()
                .map(tournament-> {
            UserTournamentDto userTournamentDto = new UserTournamentDto();
            userTournamentDto.setId(tournament.getId());
            userTournamentDto.setName(tournament.getName());
            userTournamentDto.setStartDate(tournament.getStartDate());
            userTournamentDto.setEndDate(tournament.getEndDate());
            userTournamentDto.setCity(tournament.getCity());
            userTournamentDto.setCountry(tournament.getCountry());
            userTournamentDto.setAddress(tournament.getAddress());
            userTournamentDto.setSystem1(tournament.getSystem1());
            userTournamentDto.setMaxScore(tournament.getMaxScore());
            userTournamentDto.setMinScore(tournament.getMinScore());
            userTournamentDto.setNumberRounds(tournament.getNumberRounds());
            userTournamentDto.setNumberPlayers(tournament.getNumberPlayers());
            userTournamentDto.setFinished(tournament.isFinished());

            return userTournamentDto;
        }).collect(Collectors.toList());

        List<UserTournamentDto> userTournamentDtoList1= this.getRegisteredTournaments()
                .stream()
                .map(tournament-> {
                    UserTournamentDto userTournamentDto = new UserTournamentDto();
                    userTournamentDto.setId(tournament.getId());
                    userTournamentDto.setName(tournament.getName());
                    userTournamentDto.setStartDate(tournament.getStartDate());
                    userTournamentDto.setEndDate(tournament.getEndDate());
                    userTournamentDto.setCity(tournament.getCity());
                    userTournamentDto.setCountry(tournament.getCountry());
                    userTournamentDto.setAddress(tournament.getAddress());
                    userTournamentDto.setSystem1(tournament.getSystem1());
                    userTournamentDto.setMaxScore(tournament.getMaxScore());
                    userTournamentDto.setMinScore(tournament.getMinScore());
                    userTournamentDto.setNumberRounds(tournament.getNumberRounds());
                    userTournamentDto.setNumberPlayers(tournament.getNumberPlayers());
                    userTournamentDto.setFinished(tournament.isFinished());

                    return userTournamentDto;
                }).collect(Collectors.toList());

        List<UserRoundDto> userRoundDtoList = this.getRounds()
                .stream()
                .map(round -> {
                    UserRoundDto userRoundDto = new UserRoundDto();
                    userRoundDto.setId(round.getId());
                    userRoundDto.setName(round.getName());
                    userRoundDto.setRoundStart(round.getRoundStart());
                    userRoundDto.setRoundEnd(round.getRoundEnd());
                    userRoundDto.setTournament_id(round.getTournament_id());
                    return userRoundDto;
                }).collect(Collectors.toList());

        List<UserPartiaDto> userPartiaDtos = this.getPartias()
                .stream()
                .map(partia -> {
                    UserPartiaDto userPartiaDto = new UserPartiaDto();
                    userPartiaDto.setId(partia.getId());
                    userPartiaDto.setRound_id(partia.getRound_id());
                    userPartiaDto.setName(partia.getName());
                    userPartiaDto.setTempo(partia.getTempo());
                    userPartiaDto.setStart(partia.getStart());
                    userPartiaDto.setEnd(partia.getEnd());
                    userPartiaDto.setPath(partia.getPath());
                    userPartiaDto.setWynik(partia.getWynik());
                    return userPartiaDto;
                }).collect(Collectors.toList());
        userDto.setPartias(userPartiaDtos);
        userDto.setRounds(userRoundDtoList);
        userDto.setRegisteredTournaments(userTournamentDtoList1);
        userDto.setTournaments(userTournamentDtoList);

        return userDto;
    }
    public User() { }

    public User(Long id, String userId, String username, String password, String firstName, String lastName,
                String email, String phoneNumber, String city, String country, String profileImageUrl,
                 String role, String[] authorities,
                boolean isActive, boolean isNotLocked,
                Long ranking, String title, String gender, Long age) {
        this.id = id;
        //this.userId = userId;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.city = city;
        this.country = country;
        this.profileImageUrl = profileImageUrl;
        //this.lastLoginDate = lastLoginDate;
        //LastLoginDateDisplay = lastLoginDateDisplay;
        //this.joinDate = joinDate;
        this.role = role;
        this.authorities = authorities;
        this.isActive = isActive;
        //this.isNotLocked = isNotLocked;
        this.ranking = ranking;
        this.title = title;
        this.gender = gender;
        this.age = age;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public Long getRanking() {
        return ranking;
    }

    public void setRanking(Long ranking) {
        this.ranking = ranking;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }



    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String[] getAuthorities() {
        return authorities;
    }

    public void setAuthorities(String[] authorities) {
        this.authorities = authorities;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }


}
