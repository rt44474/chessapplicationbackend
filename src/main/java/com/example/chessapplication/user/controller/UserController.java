package com.example.chessapplication.user.controller;

import com.example.chessapplication.tournament.dto.TournamentDto;
import com.example.chessapplication.tournament.dto.TournamentInputDto;
import com.example.chessapplication.user.domain.HttpResponse;
import com.example.chessapplication.user.domain.User;
import com.example.chessapplication.user.domain.UserPrincipal;
import com.example.chessapplication.user.dto.UserDto;
import com.example.chessapplication.user.dto.UserTournamentDto;
import com.example.chessapplication.user.exception.ExceptionHandling;
import com.example.chessapplication.user.exception.domain.EmailExistException;
import com.example.chessapplication.user.exception.domain.EmailNotFoundException;
import com.example.chessapplication.user.exception.domain.UserNotFoundException;
import com.example.chessapplication.user.exception.domain.UsernameExistException;
import com.example.chessapplication.user.service.UserService;
import com.example.chessapplication.user.utility.JWTTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.chessapplication.user.constant.FileConstat.*;
import static com.example.chessapplication.user.constant.SecurityConstant.JWT_TOKEN_HEADER;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.IMAGE_JPEG_VALUE;

@RestController
@RequestMapping(value = {"/","/user"})
public class UserController extends ExceptionHandling {

    private UserService userService;
    private AuthenticationManager authenticationManager;
    private JWTTokenProvider jwtTokenProvider;

    @Autowired
    public UserController(UserService userService, AuthenticationManager authenticationManager, JWTTokenProvider jwtTokenProvider) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
    }


    @PutMapping("{id}")
    //@PreAuthorize("hasAnyAuthority('user:edit')")
    public ResponseEntity<UserDto> updateRace(@RequestBody User raceInputDto, @PathVariable Long id) {
        return ResponseEntity.ok(
                userService.edit(raceInputDto).toDto()
        );
    }

    @PostMapping("/login")
    public ResponseEntity<UserDto> login(@RequestBody User user){
        authenticate(user.getUsername(), user.getPassword());
        User loginUser = userService.findUserByUsername(user.getUsername());
        UserPrincipal userPrincipal = new UserPrincipal(loginUser);
        HttpHeaders jwtHeader = getJwtHeader(userPrincipal);
        return new ResponseEntity<>(loginUser.toDto(),jwtHeader, HttpStatus.OK);
    }
    @GetMapping("/findByUsername/{username}")
    public ResponseEntity<UserDto> getUserByUsername(@PathVariable("username") String username) {
        UserDto user = userService.findUserByUsername(username).toDto();
        return new ResponseEntity<>(user,HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<User> register(@RequestParam("firstName") String firstname,
                                         @RequestParam("lastName") String lastname,
                                         @RequestParam("username") String username,
                                         @RequestParam("email") String email,
                                         @RequestParam("password") String password,
                                         @RequestParam("phoneNumber") String phoneNumber,
                                         @RequestParam("city") String city,
                                         @RequestParam("country") String country,
                                         @RequestParam("ranking") Long ranking,
                                         @RequestParam("title") String title,
                                         @RequestParam("gender") String gender,
                                         @RequestParam("age") Long age,
                                         @RequestParam(value = "profileImage") MultipartFile profileImage
                                        ) throws UserNotFoundException, EmailExistException, UsernameExistException, MessagingException, IOException {
        //System.out.println(username);
        User newUser = userService.register(firstname,lastname,username,email,password,phoneNumber,city,country,ranking,title,gender,age,profileImage);
        return new ResponseEntity<>(newUser, HttpStatus.OK);
    }

    //@PreAuthorize("hasAnyAuthority('user:update')")
    @PostMapping("/verify/{username}")
    public ResponseEntity<HttpResponse> verifyUser(@PathVariable("username") String username) throws MessagingException {
        userService.verifyUser(username);
        return response(NO_CONTENT,"User verified succesfully");
    }

    @PreAuthorize("hasAnyAuthority('user:create')")
    @PostMapping("/add")
    public ResponseEntity<User>addNewUser(@RequestParam("firstname") String firstname,
                                          @RequestParam("lastname") String lastname,
                                          @RequestParam("username") String username,
                                          @RequestParam("email") String email,
                                          @RequestParam("phoneNumber") String phoneNumber,
                                          @RequestParam("city") String city,
                                          @RequestParam("country") String country,
                                          @RequestParam("gender") String gender,
                                          @RequestParam("age") String age,
                                          @RequestParam("isNonLocked") String isNonLocked,
                                          @RequestParam("isActive") String isActive,
                                          @RequestParam("role") String role,
                                          @RequestParam(value = "profileImage", required = false) MultipartFile profileImage) throws EmailExistException, MessagingException, IOException, UsernameExistException {
        User newUser = userService.addNewUser(firstname,lastname,username,email,
                phoneNumber,city,country,gender,Long.parseLong(age),Boolean.parseBoolean(isNonLocked),Boolean.parseBoolean(isActive),role,profileImage);
        return new ResponseEntity<>(newUser, HttpStatus.OK);
    }

    //@PreAuthorize("hasAnyAuthority('user:update')")
    @PostMapping("/update")
    public ResponseEntity<User>updateUser(@RequestParam("currentUsername") String currentUsername,
                                          @RequestParam("firstname") String firstname,
                                          @RequestParam("lastname") String lastname,
                                          @RequestParam("username") String username,
                                          @RequestParam("email") String email,
                                          @RequestParam("phoneNumber") String phoneNumber,
                                          @RequestParam("city") String city,
                                          @RequestParam("country") String country,
                                          @RequestParam("gender") String gender,
                                          @RequestParam("age") String age,
                                          @RequestParam("isNonLocked") String isNonLocked,
                                          @RequestParam("isActive") String isActive,
                                          @RequestParam("role") String role,
                                          @RequestParam(value = "profileImage", required = false) MultipartFile profileImage) throws EmailExistException, MessagingException, IOException, UsernameExistException {
        User updatedUser = userService.updateUser(currentUsername,username,firstname,lastname,email,phoneNumber,city,
                country,Long.parseLong(age),Boolean.parseBoolean(isNonLocked),Boolean.parseBoolean(isActive),
                role,profileImage);
        return new ResponseEntity<>(updatedUser, HttpStatus.OK);
    }

    @PostMapping("/resetPassword")
    public ResponseEntity<User> resetUserPassword(@RequestParam("email") String email,
                                                  @RequestParam("password") String password) throws EmailNotFoundException, MessagingException {
        User newUser = userService.resetPassword(email,password);
        return new ResponseEntity<>(newUser,HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    //@PreAuthorize("hasAnyAuthority('user:delete')")
    public ResponseEntity<HttpResponse> deleteUser(@PathVariable("id") Long id){
        userService.deleteUser(id);
        return response(NO_CONTENT,"User deleted succesfully");
    }

    @PostMapping("/updateProfileImage")
    public ResponseEntity<User>updateProfileImage(@RequestParam("username") String username,
                                                  @RequestParam(value = "profileImage") MultipartFile profileImage) throws EmailExistException, MessagingException, IOException, UsernameExistException {
        User user = userService.updateProfileImage(username,profileImage);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    private ResponseEntity<HttpResponse> response(HttpStatus httpStatus, String message){

        return new ResponseEntity<>(new HttpResponse(httpStatus.value(),httpStatus,httpStatus.getReasonPhrase().toUpperCase(),
                message.toUpperCase()),httpStatus);
    }

    @GetMapping(path = "/image/{username}/{fileName}",produces = IMAGE_JPEG_VALUE)
    public byte[] getProfileImage(@PathVariable("username") String username,
                                  @PathVariable("fileName") String fileName) throws IOException {
        return Files.readAllBytes(Paths.get(USER_FOLDER + username + FORWARD_SLASH + fileName));
    }

    @GetMapping(path = "/image/profile/{username}",produces = IMAGE_JPEG_VALUE)
    public byte[] getTempProfileImage(@PathVariable("username") String username) throws IOException {
        URL url = new URL(TEMP_PROFILE_IMAGE_BASE_URL + username);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try (InputStream inputStream = url.openStream()) {
            int bytesRead;
            byte[] chunk = new byte[1024];
            while((bytesRead = inputStream.read(chunk)) > 0) {
                byteArrayOutputStream.write(chunk,0,bytesRead);
            }
        }
        return byteArrayOutputStream.toByteArray();
    }



    @GetMapping("/list")
    public ResponseEntity<List<UserDto>> getAllUsers() {
        ResponseEntity<List<UserDto>> a = ResponseEntity.ok(userService
                .getUsers()
                .stream()
                .map(tournament -> tournament.toDto())
                .collect(Collectors.toList()));
        return ResponseEntity.ok(userService
            .getUsers()
            .stream()
            .map(tournament -> tournament.toDto())
            .collect(Collectors.toList()));
    }

    @GetMapping("/findByEmail/{email}")
    public ResponseEntity<User> getUserByEmail(@PathVariable("email") String email) {
        User user = userService.findUserByEmail(email);
        return new ResponseEntity<>(user,HttpStatus.OK);
    }

    @PatchMapping("/updateStats/{name}")
    public ResponseEntity<UserDto> updateStats(@RequestParam("win") Long win,
                                               @RequestParam("draw") Long draw,
                                               @RequestParam("lose") Long lose,
                                               @PathVariable String name){
        return ResponseEntity.ok(
                userService.updateStats(name,win,draw,lose).toDto()
        );
    }

    @PatchMapping("/updateStats1/{name}")
    public ResponseEntity<UserDto> updateStats1(@RequestParam("win") Long punkty, @PathVariable String name){
        return ResponseEntity.ok(
                userService.updateStats1(name,punkty).toDto()
        );
    }






    private HttpHeaders getJwtHeader(UserPrincipal user) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(JWT_TOKEN_HEADER,jwtTokenProvider.generateJwtToken(user));
        return headers;
    }

    private void authenticate(String username, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username,password));
    }
}
