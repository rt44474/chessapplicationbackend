package com.example.chessapplication.user.configuration;

import com.example.chessapplication.user.constant.SecurityConstant;
import com.example.chessapplication.user.domain.User;
import com.example.chessapplication.user.enumeration.Role;
import com.example.chessapplication.user.filter.JwtAccesDeniedHandler;
import com.example.chessapplication.user.filter.JwtAuthenticationEntryPoint;
import com.example.chessapplication.user.filter.JwtAuthorizationFilter;
import com.example.chessapplication.user.repository.UserRepository;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private JwtAuthorizationFilter jwtAuthorizationFilter;
    private JwtAccesDeniedHandler jwtAccesDeniedHandler;
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
    private UserDetailsService userDetailsService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private Environment env;
    private UserRepository userRepository;

    @Autowired
    public SecurityConfiguration(JwtAuthorizationFilter jwtAuthorizationFilter,
                                 JwtAccesDeniedHandler jwtAccesDeniedHandler,
                                 JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint,
                                 @Qualifier("UserDetailsService")UserDetailsService userDetailsService,
                                 BCryptPasswordEncoder bCryptPasswordEncoder,
                                 Environment env,
                                 UserRepository userRepository) {
        this.jwtAuthorizationFilter = jwtAuthorizationFilter;
        this.jwtAccesDeniedHandler = jwtAccesDeniedHandler;
        this.jwtAuthenticationEntryPoint = jwtAuthenticationEntryPoint;
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userRepository = userRepository;
        this.env = env;
    }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().cors().and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests().antMatchers(SecurityConstant.PUBLIC_URLS)
        .permitAll()
        .anyRequest().authenticated()
        .and()
        .exceptionHandling().accessDeniedHandler(jwtAccesDeniedHandler)
        .authenticationEntryPoint(jwtAuthenticationEntryPoint)
        .and()
        .addFilterBefore(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class);
    }
    @Bean
    public CorsFilter corsFilter(){

        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();

        corsConfiguration.setAllowCredentials(true);
        corsConfiguration.setAllowedOrigins(Collections.singletonList("http://localhost:4200"));
        corsConfiguration.setAllowedHeaders(Arrays.asList("Origin", "Access-Control-Allow-Origin", "Content-Type",
                "Accept", "Jwt-Token", "Authorization", "Origin, Accept", "X-Requested-With",
                "Access-Control-Request-Method", "Access-Control-Request-Headers"));

        corsConfiguration.setExposedHeaders(Arrays.asList("Origin", "Content-Type", "Accept", "Jwt-Token", "Authorization",
                "Access-Control-Allow-Origin", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials"));

        corsConfiguration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH"));

        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);

        return new CorsFilter(urlBasedCorsConfigurationSource);


        /*CorsConfiguration config = new CorsConfiguration();

        config.addAllowedOrigin(env.getProperty("allowed-origin"));
        config.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
        config.addAllowedHeader("Authorization");
        config.addAllowedHeader("Content-Type");
        config.addAllowedHeader("X-Requested-With");

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);

        return new CorsFilter(source);*/
    }
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception{
        return super.authenticationManagerBean();
    }

    @Bean
    public CommandLineRunner commandLineRunner(){
        return args -> {
            if(this.userRepository.count() == 0) {
                User user = new User();
                user.setUsername("admin");
                user.setRole(Role.ROLE_ADMIN.name());
                user.setActive(true);
                //user.setNotLocked(true);
                user.setAuthorities(Role.ROLE_ADMIN.getAuthorities());
                //admin:admin
                user.setPassword("$2a$12$AezBvNo/Sim70EqHa0w3QepcPgOkBCSruTCPDx8NmhBSoGz7cQeoe");
                this.userRepository.save(user);
                User user1 = new User();
                user1.setUsername("fxtomek");
                user1.setFirstName("Tomasz");
                user1.setLastName("Rafalski");
                user1.setActive(true);
                //user1.setNotLocked(true);
                user1.setRole(Role.ROLE_ORGANISER.name());
                user1.setPassword("$2a$12$AezBvNo/Sim70EqHa0w3QepcPgOkBCSruTCPDx8NmhBSoGz7cQeoe");
                user1.setAuthorities(Role.ROLE_ORGANISER.getAuthorities());
                this.userRepository.save(user1);
                User user2 = new User();
                user2.setUsername("fxtomek1");
                user2.setFirstName("Klaudia");
                user2.setLastName("Stanko");
                user2.setActive(true);
                //user2.setNotLocked(true);
                user2.setRole(Role.ROLE_JUDGE.name());
                user2.setAuthorities(Role.ROLE_JUDGE.getAuthorities());
                user2.setPassword("$2a$12$AezBvNo/Sim70EqHa0w3QepcPgOkBCSruTCPDx8NmhBSoGz7cQeoe");
                this.userRepository.save(user2);

            }
        };
    }


}
