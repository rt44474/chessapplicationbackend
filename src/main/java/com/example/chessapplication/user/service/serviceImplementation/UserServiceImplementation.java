package com.example.chessapplication.user.service.serviceImplementation;

import com.example.chessapplication.tournament.domain.Tournament;
import com.example.chessapplication.tournament.dto.TournamentDto;
import com.example.chessapplication.tournament.dto.TournamentInputDto;
import com.example.chessapplication.tournament.mappers.TournamentMapper;
import com.example.chessapplication.user.domain.User;
import com.example.chessapplication.user.domain.UserPrincipal;
import com.example.chessapplication.user.dto.UserDto;
import com.example.chessapplication.user.dto.UserTournamentDto;
import com.example.chessapplication.user.enumeration.Role;
import com.example.chessapplication.user.exception.domain.EmailExistException;
import com.example.chessapplication.user.exception.domain.EmailNotFoundException;
import com.example.chessapplication.user.exception.domain.UsernameExistException;
import com.example.chessapplication.user.mappers.UserMappers;
import com.example.chessapplication.user.repository.UserRepository;
import com.example.chessapplication.user.service.EmailService;
import com.example.chessapplication.user.service.LoginAttemptService;
import com.example.chessapplication.user.service.UserService;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import static com.example.chessapplication.user.constant.FileConstat.*;
import static com.example.chessapplication.user.constant.UserImpConstant.*;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;


@Service
@Transactional
@Qualifier("UserDetailsService")
public class UserServiceImplementation implements UserService, UserDetailsService {


    private Logger LOGGER = LoggerFactory.getLogger(getClass());
    private UserRepository userRepository;
    private BCryptPasswordEncoder passwordEncoder;
    private EmailService emailService;
    private LoginAttemptService loginAttemptService;
    private UserMappers userMappers  = Mappers.getMapper(UserMappers.class);

    @Autowired
    public UserServiceImplementation(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder, EmailService emailService, LoginAttemptService loginAttemptService) {
        this.userRepository = userRepository;
        this.passwordEncoder =passwordEncoder;
        this.emailService = emailService;
        this.loginAttemptService = loginAttemptService;

    }

    @Override
    public User edit(User userTournamentDto) {
        User tournament = new User();
        tournament = userTournamentDto;
        userRepository.save(tournament);
        return tournament;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findUserByUsername(username);
        if (user == null){
            LOGGER.error(NO_USER_FOUND_BY_USERNAME + username);
            throw new UsernameNotFoundException(NO_USER_FOUND_BY_USERNAME + username);

        } else {
            validateLoginAttempt(user);
            //user.setLastLoginDateDisplay(user.getLastLoginDate());
            //user.setLastLoginDate(new Date());
            userRepository.save(user);
            UserPrincipal userPrincipal = new UserPrincipal(user);
            LOGGER.info(RETURNING_FOUND_USER_BY_USERNAME + username);

            return userPrincipal;
        }
    }

    private void validateLoginAttempt(User user){
        /*if(user.isNotLocked()){
            if(loginAttemptService.hasExceedMaxAttempts(user.getUsername())){
                user.setNotLocked(false);
            }else {
                user.setNotLocked(true);
            }
        } else {
            loginAttemptService.evictUserFromLoginAttemptCache(user.getUsername());
        }*/
    }

    @Override
    public User register(String firstname, String lastname, String username, String email, String password, String phoneNumber, String city, String country, Long ranking, String title, String gender, Long age, MultipartFile image) throws EmailExistException, UsernameExistException, MessagingException, IOException {
        validateNewUserNameAndEmail(StringUtils.EMPTY, username, email);
        User user = new User();
        //user.setUserId(generateUserId());
        //String password = generatePassword();
        String encodedPassword = encodePassword(password);
        user.setFirstName(firstname);
        user.setLastName(lastname);
        user.setUsername(username);
        user.setPhoneNumber(phoneNumber);
        user.setCity(city);
        user.setCountry(country);
        user.setRanking(ranking);
        user.setTitle(title);
        System.out.println("GENDER:!!!!!!!!!!!!");
        System.out.println(gender);
        user.setGender(gender);
        user.setAge(age);
        user.setEmail(email);
        user.setJoinDate((new Date()));
        user.setPassword(encodedPassword);
        user.setActive(false);
        //user.setNotLocked(true);
        user.setRole(Role.ROLE_USER.name());
        user.setAuthorities(Role.ROLE_USER.getAuthorities());
        user.setProfileImageUrl(getTemporaryProfileImageUrl(username));
        user.setPunkty(0);
        user.setZwyciestwa(0);
        user.setRemisy(0);
        user.setPorazki(0);
        userRepository.save(user);
        saveProfileImage(user,image);
        LOGGER.info("New user password: " + password);
        //emailService.sendEmail(firstname,email);
        return user;
    }


    @Override
    public User addNewUser(String firstname, String lastname, String username, String email, String phoneNumber, String city, String country,String gender,Long age, boolean isNonLocked, boolean isActive, String role, MultipartFile profileImage) throws EmailExistException, UsernameExistException, MessagingException, IOException {
        validateNewUserNameAndEmail("",username,email);
        User user = new User();
        String password = generatePassword();
        System.out.println("Password: "+ password);
        String encodedPassword = encodePassword(password);
        //user.setUserId(generateUserId());
        user.setFirstName(firstname);
        user.setLastName(lastname);
        user.setJoinDate(new Date());
        user.setUsername(username);
        user.setAge(age);
        user.setGender(gender);
        user.setCountry(country);
        user.setCity(city);
        user.setPhoneNumber(phoneNumber);
        user.setEmail(email);
        user.setPassword(encodedPassword);
        user.setActive(isActive);
        //user.setNotLocked(isNonLocked);
        user.setRole(getRoleEnumName(role).name());
        user.setAuthorities(getRoleEnumName(role).getAuthorities());
        user.setProfileImageUrl(getTemporaryProfileImageUrl(username));
        user.setPunkty(0);
        user.setZwyciestwa(0);
        user.setRemisy(0);
        user.setPorazki(0);
        userRepository.save(user);
        saveProfileImage(user,profileImage);
        emailService.sendEmail1(firstname,password,email);
        return user;


    }

    @Override
    public User updateUser(String currentUsername,String newUsername, String newFirstName, String newLastName, String newEmail, String newPhoneNumber, String newCity, String newCountry,Long newAge, boolean isNonLocked, boolean isActive, String role, MultipartFile profileImage) throws EmailExistException, UsernameExistException, IOException {
        User currentuser = validateNewUserNameAndEmail(currentUsername,newUsername,newEmail);
        currentuser.setFirstName(newFirstName);
        currentuser.setLastName(newLastName);
        currentuser.setUsername(newUsername);
        currentuser.setAge(newAge);
        currentuser.setCountry(newCountry);
        currentuser.setCity(newCity);
        currentuser.setPhoneNumber(newPhoneNumber);
        currentuser.setEmail(newEmail);
        currentuser.setActive(isActive);
        //currentuser.setNotLocked(isNonLocked);
        currentuser.setRole(getRoleEnumName(role).name());
        currentuser.setAuthorities(getRoleEnumName(role).getAuthorities());
        userRepository.save(currentuser);
        saveProfileImage(currentuser,profileImage);
        return currentuser;
    }

    @Override
    public User verifyUser(String username) throws MessagingException {

        User user = userRepository.findUserByUsername(username);
        user.setActive(true);
        userRepository.save(user);
        String encodedPassword = encodePassword(user.getPassword());
        emailService.sendEmail(user.getFirstName(),user.getEmail());
        return user;
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User resetPassword(String email, String password) throws EmailNotFoundException, MessagingException {
        User user = userRepository.findUserByEmail(email);
        if (user == null) {
            throw new EmailNotFoundException(NO_USER_FOUND_BY_EMAIL + email);
        }
        String encodedPassword = encodePassword(password);
        user.setPassword(encodedPassword);
        userRepository.save(user);
        emailService.sendNewPasswordEmail(user.getFirstName(),email);
        return user;
    }

    private void saveProfileImage(User user, MultipartFile profileImage) throws IOException {
        if (profileImage != null){
            Path userFolder = Paths.get(USER_FOLDER + user.getUsername()).toAbsolutePath().normalize();
            if (!Files.exists(userFolder)) {
                Files.createDirectories(userFolder);
                LOGGER.info(DIRECTORY_CREATED + userFolder);
            }
            Files.deleteIfExists(Paths.get(userFolder + user.getUsername() + DOT + JPG_EXTENSION));
            Files.copy(profileImage.getInputStream(),userFolder.resolve(user.getUsername()+DOT+JPG_EXTENSION), REPLACE_EXISTING);
            user.setProfileImageUrl(setProfileImageUrl(user.getUsername()));
            userRepository.save(user);
            LOGGER.info(FILE_SAVED_IN_FILE_SYSTEM + profileImage.getOriginalFilename());


        }
    }

    private String setProfileImageUrl(String username) {
        return ServletUriComponentsBuilder.fromCurrentContextPath().path(USER_IMAGE_PATH + username + FORWARD_SLASH + username + DOT + JPG_EXTENSION)
                .toUriString();
    }

    private Role getRoleEnumName(String role) {
        return Role.valueOf(role.toUpperCase());
    }

    private String getTemporaryProfileImageUrl(String username) {
        return ServletUriComponentsBuilder.fromCurrentContextPath().path(DEFAULT_USER_IMAGE_PATH + username)
                .toUriString();
    }

    private String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    private String generatePassword() {
        return RandomStringUtils.randomAlphanumeric(10);
    }

    private String generateUserId() {
        return RandomStringUtils.randomNumeric(10);
    }

    private User validateNewUserNameAndEmail(String currentUsername, String newUsername, String newEmail) throws UsernameExistException, EmailExistException {
        User userByNewUsername = findUserByUsername(newUsername);
        User userByNewEmail = findUserByEmail(newEmail);
        if(StringUtils.isNotBlank(currentUsername)){
            User currentUser = findUserByUsername(currentUsername);
            if (currentUser == null) {
                throw new UsernameNotFoundException(NO_USER_FOUND_BY_USERNAME+currentUsername);
            }
            if(userByNewUsername != null && !currentUser.getId().equals(userByNewUsername.getId())) {
                throw new UsernameExistException(USERNAME_ALREADY_EXIST);
            }
            if(userByNewEmail != null && !currentUser.getId().equals(userByNewEmail.getId())) {
                throw new EmailExistException(EMAIL_ALREADY_EXIST);
            }
            return currentUser;
        } else {
            if (userByNewUsername != null){
                throw new UsernameExistException(USERNAME_ALREADY_EXIST);
            }
            if(userByNewEmail != null ) {
                throw new EmailExistException(EMAIL_ALREADY_EXIST);
            }
            return null;
        }
    }

    @Override
    public List<User> getUsers() {
        List<User> a = userRepository.findAll();
        return userRepository.findAll();
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    @Override
    public User updateStats(String name, Long win, Long draw, Long lose){
        User user1 = userRepository.findUserByUsername(name);
        user1.setZwyciestwa(user1.getZwyciestwa() + win);
        user1.setRemisy(user1.getRemisy() + draw);
        user1.setPorazki(user1.getPorazki() + lose);
        userRepository.save(user1);
        return user1;

    }

    @Override
    public User updateStats1(String name, Long punkty){
        User user1 = userRepository.findUserByUsername(name);
        user1.setPunkty(user1.getPunkty() + punkty);
        userRepository.save(user1);
        return user1;

    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Override
    public User updateProfileImage(String username, MultipartFile profileImage) throws EmailExistException, UsernameExistException, IOException {
        User user = validateNewUserNameAndEmail(username,null, null);
        saveProfileImage(user,profileImage);
        return user;
    }


}
