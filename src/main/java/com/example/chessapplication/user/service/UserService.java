package com.example.chessapplication.user.service;


import com.example.chessapplication.tournament.dto.TournamentDto;
import com.example.chessapplication.tournament.dto.TournamentInputDto;
import com.example.chessapplication.user.domain.User;
import com.example.chessapplication.user.dto.UserDto;
import com.example.chessapplication.user.dto.UserTournamentDto;
import com.example.chessapplication.user.exception.domain.EmailExistException;
import com.example.chessapplication.user.exception.domain.EmailNotFoundException;
import com.example.chessapplication.user.exception.domain.UsernameExistException;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

public interface UserService {
    User edit(User tournament);
    User register(String firstname,
                  String lastname,
                  String username,
                  String email,
                  String password,
                  String phoneNumber,
                  String city,
                  String country,
                  Long ranking,
                  String title,
                  String gender,
                  Long age,
                  MultipartFile image) throws EmailExistException, UsernameExistException, MessagingException, IOException;

    List<User> getUsers();

    User findUserByUsername(String username);

    User findUserByEmail(String email);

    User addNewUser(String firstname,
                    String lastname,
                    String username,
                    String email,
                    String phoneNumber,
                    String city,
                    String country,
                    String gender,
                    Long age,
                    boolean isNonLocked,
                    boolean isActive,
                    String role,
                    MultipartFile profileImage) throws EmailExistException, UsernameExistException, MessagingException, IOException;

    User updateUser(String currentUsername,
                    String newUsername,
                    String newFirstName,
                    String newLastName,
                    String newEmail,
                    String newPhoneNumber,
                    String newCity,
                    String newCountry,
                    Long newAge,
                    boolean isNonLocked,
                    boolean isActive,
                    String role,
                    MultipartFile profileImage) throws EmailExistException, UsernameExistException, IOException;

    User verifyUser(String username) throws MessagingException;

    void deleteUser(Long id);

    User resetPassword(String email, String password) throws EmailNotFoundException, MessagingException;

    User updateProfileImage(String username, MultipartFile profileImage) throws EmailExistException, UsernameExistException, IOException;

    User updateStats(String name, Long win, Long draw, Long lose);
    User updateStats1(String name, Long punkty);




}
